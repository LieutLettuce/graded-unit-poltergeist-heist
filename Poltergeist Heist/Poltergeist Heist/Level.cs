﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;

namespace Poltergeist_Heist
{
    class Level : Screen
    {
        // ------------------
        // Data
        // ------------------
        private Player player;
        private Portal ourPortal;
        private Wall[,] walls;
        private Tile[,] floorTiles;
        private Tile[,] tiles;
        private Boulder[,] boulders;
        //private Enemy[,] enemies;
        private Enemy enemy;
        //private List<Boulder> boulders = new List<Boulder>();
        private List<Enemy> enemies = new List<Enemy>();
        private List<Coin> coins = new List<Coin>();
        private List<Spike> spikes = new List<Spike>();

        // Level Related 
        private const int FINAL_LEVEL = 3;
        private bool loadNextLevel = false;


        private Rectangle healthRectangle;
        private Rectangle backdropRectangle;
        private Rectangle backdropRectangle2;

        private int health;
        private bool isDead = false;
        private float timeSinceDeath = 0;
        private const float END_SCREEN_COOLDOWN = 0.25f;

        private int tileWidth;
        private int tileHeight;
        private int levelWidth;
        private int levelHeight;
        private Text scoreDisplay;
        private int currentLevel;
        private bool reloadLevel = false;
        private Text livesDiscplay;
        private Game1 game;

        private Camera ourCamera;

        // Assets
        private Texture2D playerSprite;
        private Texture2D enemySprite;
        private Texture2D healthSprite;
        private Texture2D backdropSprite;
        private Texture2D portalSprite;
  
        private Texture2D wallSprite;
        private Texture2D coinSprite;
        private Texture2D spikeSprite;
        private Texture2D floorSprite;
        private Texture2D boulderSprite;

        private SoundEffect playerDeath;
        private SoundEffect playerFootstep;

        private SoundEffectInstance playerDeathInstance;

        private SoundEffectInstance gameWinInstance;

        private SoundEffect gameWin;
        private Song gameBackground;

        private SpriteFont UIFont;

        private Vector2 newPosition;

        // ------------------
        // Behaviour
        // ------------------

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            //Game Audio Variables
            playerDeath = content.Load<SoundEffect>("audio/deathh");  // Load The sound for when the player dies.
            playerFootstep = content.Load<SoundEffect>("audio/footstep"); //Load The player's footstep sound.
            gameWin = content.Load<SoundEffect>("audio/PG_Win"); // Load the sound effect for winning the game.
            gameBackground = content.Load<Song>("audio/PG_Background"); // This is the ambient sound of the game as it plays.

            playerDeathInstance = playerDeath.CreateInstance(); //This is the instance that will be used for the death sfx

            gameWinInstance = gameWin.CreateInstance(); // This is the instance for the player win sfx


            //Foreground Sprites
            playerSprite = content.Load<Texture2D>("graphics/ScientistSheetTEST3");
            enemySprite = content.Load<Texture2D>("graphics/EnemySprite");
            wallSprite = content.Load<Texture2D>("graphics/Tree16Bit");
            coinSprite = content.Load<Texture2D>("graphics/Parchment2");
            spikeSprite = content.Load<Texture2D>("graphics/Spike");
            boulderSprite = content.Load<Texture2D>("graphics/boulder2");
            portalSprite = content.Load<Texture2D>("graphics/portal");

            //Background Sprites
            floorSprite = content.Load<Texture2D>("graphics/Floor");

            //UI Sprites
            healthSprite = content.Load<Texture2D>("graphics/Health Bar");
            backdropSprite = content.Load<Texture2D>("graphics/Backdrop");

            tileWidth = wallSprite.Width;
            tileHeight = wallSprite.Height;

            // Set up UI
            UIFont = content.Load<SpriteFont>("fonts/mainFont");
            scoreDisplay = new Text(UIFont);
            scoreDisplay.SetPosition(new Vector2(10, 10));

            livesDiscplay = new Text(UIFont);
            livesDiscplay.SetPosition(new Vector2(graphics.Viewport.Bounds.Width - 10, 10));
            livesDiscplay.SetAlignment(Text.Alignment.TOP_RIGHT);

            // Create the player once, we'll move them later
            player = new Player(playerSprite, tileWidth, tileHeight, 6, this, 100);
            ourPortal = new Portal(portalSprite);

            // Create our camera
            ourCamera = new Camera(player, new Vector2(Game1.ScreenWidth,
                Game1.ScreenHeight));

            MediaPlayer.Play(gameBackground);

            // TEMP - this will be moved later
            LoadLevel(1);

            player.LoadContent(content, graphics);
            ourPortal.LoadContent(content, graphics);

        }
        // ------------------
        public void LoadLevel(int levelNum)
        {
            currentLevel = levelNum;
            string baseLevelName = "Levels/level_";
            LoadLevel(baseLevelName + levelNum.ToString() + ".txt");
        }
        // ------------------
        public Level(Game1 newGame)
        {
            game = newGame;
        }
        // ------------------
        public void PositionPlayer(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            player.SetPosition(tilePosition);
        }
        // ------------------
        public void PositionPortal(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            ourPortal.SetPosition(tilePosition);
            ourPortal.SetPlayer(player);
        }
        // ------------------
        private void CreateWall(int tileX, int tileY)
        {
            Wall wall = new Wall(wallSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            wall.SetPosition(tilePosition);
            walls[tileX, tileY] = wall;
        }
        // ------------------
        private void CreateEnemy(int tileX, int tileY)
        {
            Enemy newEnemy = new Enemy(enemySprite, this, 0, newPosition );
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newEnemy.SetPosition(tilePosition);
            enemies.Add(newEnemy);
        }
        // ------------------
        private void CreateCoin(int tileX, int tileY)
        {
            Coin newCoin = new Coin(coinSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newCoin.SetPosition(tilePosition);
            coins.Add(newCoin);
        }
        // ------------------
        private void CreateSpike(int tileX, int tileY)
        {
            Spike newTile = new Spike(spikeSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newTile.SetPosition(tilePosition);
            spikes.Add(newTile);
        }
        // ------------------
        private void CreateBoulder(int tileX, int tileY)
        {
            Boulder boulder = new Boulder(boulderSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            boulder.SetPosition(tilePosition);
            boulders[tileX, tileY] = boulder;
        }
        // ------------------
        private void CreateFloor(int tileX, int tileY)
        {
            Floor tile = new Floor(floorSprite);
            tile.SetTilePosition(new Vector2(tileX, tileY));
            floorTiles[tileX, tileY] = tile;
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // Update all actors

            ourCamera.Update();
            player.Update(gameTime);
            PlayerDeath();
            ourPortal.Update(gameTime);


            healthRectangle = new Rectangle(5, 52, player.GetHealth(), 20);
            backdropRectangle = new Rectangle(5, 8, 120, 20);
            backdropRectangle2 = new Rectangle(5, 30, 120, 20);

            // if the player has died then display the end screen  
            if (isDead == true)
            {
                timeSinceDeath = 0;
                game.ChangeScreen("end");
            }

            if (isDead == true && (keyboardState.IsKeyDown(Keys.R)))
            {
                player.ResetHealth();
                reloadLevel = true;
                isDead = false;
                
            }

            foreach (Tile tile in floorTiles)
            {
                if (tile != null)
                    tile.Update(gameTime);
            }
            
            foreach (Enemy eachEnemy in enemies)
            {
                if (eachEnemy != null)
                    eachEnemy.Update(gameTime);
            }
            

            // Collision checking

            // Walls collisions
            List<Wall> collidingWalls = GetTilesInBounds(player.GetBounds());
            foreach(Wall collidingWall in collidingWalls)
            {
                player.HandleCollision(collidingWall);
            }

            
            // Enemy collisions
            foreach (Enemy eachEnemy in enemies)
            {
                if (!reloadLevel && eachEnemy.GetVisible() == true && eachEnemy.GetBounds().Intersects(player.GetBounds()))
                {
                    player.HandleCollision(eachEnemy);
                }
                    
            }
            

            // Boulders collisions
            List<Boulder> collidingBoulders = GetTilesInBoundsBoulder(player.GetBounds());
            foreach (Boulder collidingBoulder in collidingBoulders)
            {
                player.HandleCollision(collidingBoulder);
            }

            // Coin collisions
            foreach (Coin eachCoin in coins)
            {
                if (!reloadLevel && eachCoin.GetVisible() == true && eachCoin.GetBounds().Intersects(player.GetBounds()))
                    player.HandleCollision(eachCoin);
            }

            // Spike collisions
            foreach (Spike eachSpike in spikes)
            {
                if (!reloadLevel && eachSpike.GetVisible() == true && eachSpike.GetBounds().Intersects(player.GetBounds()))
                    player.HandleCollision(eachSpike);
            }

            // Portal Collisions
                if (!reloadLevel && ourPortal.GetisVisible() == true && ourPortal.GetBounds().Intersects(player.GetBounds()))
                {
                  player.HandleCollision(ourPortal);
                  Victory();
                }
                    

            // Update score
            scoreDisplay.SetTextString("Score: " + player.GetScore());

            // Update lives
            livesDiscplay.SetTextString("Lives: " + player.GetLives());

            // Check if we need to reload the level
            if (reloadLevel == true)
            {
                LoadLevel(currentLevel);
                reloadLevel = false;
            }

            // If we were waiting to load a new level, do it now
            if (loadNextLevel == true)
            {
                if (currentLevel == FINAL_LEVEL)
                {
                    LoadLevel(1); // restart so they can load again
                    game.ChangeScreen("finish");
                }
                else
                {
                    LoadLevel(currentLevel + 1);
                }
                loadNextLevel = false;
            }
        }
        // ------------------
        public List<Wall> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Wall> tilesInBounds = new List<Wall>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / tileWidth);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / tileWidth) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / tileHeight);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / tileHeight) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    // Only add the tile if it exists (is not null)
                    // And if it is visible
                    Wall thisTile = GetTile(x, y);

                    if (thisTile != null && thisTile.GetVisible() == true)
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }
        // ------------------
        public List<Boulder> GetTilesInBoundsBoulder(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Boulder> tilesInBounds = new List<Boulder>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / tileWidth);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / tileWidth) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / tileHeight);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / tileHeight) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    // Only add the tile if it exists (is not null)
                    // And if it is visible
                    Boulder thisTile = GetTileBoulder(x, y);

                    if (thisTile != null && thisTile.GetVisible() == true)
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }
        // ------------------

        public Wall GetTile(int x, int y)
        {
            // Check if we are out of bounds
            if (x < 0 || x >= levelWidth || y < 0 || y >= levelHeight)
                return null;

            // Otherwise we are within the bounds
            return walls[x, y];
        }
        // ------------------
        public Boulder GetTileBoulder(int x, int y)
        {
            // Check if we are out of bounds
            if (x < 0 || x >= levelWidth || y < 0 || y >= levelHeight)
                return null;

            // Otherwise we are within the bounds
            return boulders[x, y];
        }
        // ------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            //Draw The Camera
            ourCamera.Begin(spriteBatch);

            //Draw The Floors Tiles
            foreach (Tile tile in floorTiles)
            {
                if (tile != null)
                    tile.Draw(spriteBatch);
            }

            //Draw The Player
            player.Draw(spriteBatch);

            //Draw The Portal When The Scroll Is Collected
            if (ourPortal.GetisVisible() == true)
            {
                ourPortal.Draw(spriteBatch);
            }
            //Drawing The Walls
            foreach (Wall eachWall in walls)
            {
                if (eachWall != null)
                    eachWall.Draw(spriteBatch);
            }

            //Drawing The Coins
            foreach (Coin eachCoin in coins)
            {
                eachCoin.Draw(spriteBatch);
            }

            //Drawing Enemies
            foreach (Enemy eachEnemy in enemies)
            {
                if (eachEnemy != null)
                    eachEnemy.Draw(spriteBatch);
            }

            //Drawing Spikes
            foreach (Spike eachSpike in spikes)
            {
                eachSpike.Draw(spriteBatch);
            }

            //Drawing Boulders
            foreach (Boulder eachBoulder in boulders)
            {
                if (eachBoulder != null)
                    eachBoulder.Draw(spriteBatch);
            }

            //End Of Draw Function
            spriteBatch.End();


            //UI Draw Function
            spriteBatch.Begin();
            spriteBatch.Draw(healthSprite, healthRectangle, Color.White);
            spriteBatch.Draw(backdropSprite, backdropRectangle, Color.White);
            spriteBatch.Draw(backdropSprite, backdropRectangle2, Color.White);
            spriteBatch.DrawString(UIFont,"SCORE:  " + player.GetScore(), new Vector2(10,10), Color.White);
            spriteBatch.DrawString(UIFont, "HEALTH:  " + player.GetHealth(), new Vector2(10,30), Color.White);
            spriteBatch.End();
            
        }
        // ------------------
        public void LoadLevel(string fileName)
        {
            // Clear any existing level data
            ClearLevel();

            // Create filestream to open the file and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // Before we read in the individual tiles in the level, we need to know 
            // how big the level is overall to create the arrays to hold the data
            int lineWidth = 0; // Eventually will be levelWidth
            int numLines = 0;  // Eventually will be levelHeight
            List<string> lines = new List<string>();    // this will contain all the strings of text in the file
            StreamReader reader = new StreamReader(fileStream); // This will let us read each line from the file
            string line = reader.ReadLine(); // Get the first line
            lineWidth = line.Length; // Assume the overall line width is the same as the length of the first line
            while (line != null) // For as long as line exists, do something
            {
                lines.Add(line); // Add the current line to the list
                if (line.Length != lineWidth)
                {
                    // This means our lines are different sizes and that is a big problem
                    throw new Exception("Lines are different widths - error occured on line "+lines.Count);
                }

                // Read the next line to get ready for the next step in the loop
                line = reader.ReadLine();
            }

            // We have read in all the lines of the file into our lines list
            // We can now know how many lines there were
            numLines = lines.Count;

            // Now we can set up our tile array
            levelWidth = lineWidth;
            levelHeight = numLines;

            walls = new Wall[levelWidth, levelHeight];
            boulders = new Boulder[levelWidth, levelHeight];
            //enemies = new Enemy[levelWidth, levelHeight];

            floorTiles = new Tile[lineWidth, numLines]; 


            // Loop over every tile position and check the letter
            // there and load a tile based on  that letter
            for (int y = 0; y < levelHeight; ++y)
            {
                for (int x = 0; x < levelWidth; ++x)
                {
                    // Load each tile
                    char tileType = lines[y][x];
                    // Load the tile
                    LoadTile(tileType, x, y);
                }
            }

            // Verify that the level is actually playable
            if (player == null)
            {
                throw new NotSupportedException("A level must have a starting point for the player.");
            }
        }
        // ------------------
        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                // Player
                case 'P':
                    PositionPlayer(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Wall
                case 'W':
                    CreateWall(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                //Boulder
                case 'B':
                    CreateBoulder(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                //Enemy
                case 'E':
                    CreateEnemy(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Coin
                case 'C':
                    CreateCoin(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Spike
                case 'S':
                    CreateSpike(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Portal
                case 'D':
                    PositionPortal(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Blank space
                case '.':
                    CreateFloor(tileX, tileY);
                    break; // Do nothing

                // Any non-handled symbol
                default:
                    throw new NotSupportedException("Level contained unsupported symbol "+tileType+" at line "+tileY+" and character "+tileX);
            }
        }
        // ------------------
        public Tile GetTileAtPosition(Vector2 tilePos)
        {
            // Check if the position is within bounds
            int posX = (int)tilePos.X;
            int posY = (int)tilePos.Y;
            if (posX >= 0
                && posY >= 0
                && posX < tiles.GetLength(0) // gets the length in the X direction
                && posY < tiles.GetLength(1)) // gets the array length in the Y direction
            {
                // Yes, this coordinate is legal
                return tiles[posX, posY];
            }
            else
            {
                // NO, this coordinate is NOT legal (out of bounds of array / tile grid)
                return null;
            }
        }
        // ------------------
        public Tile GetFloorAtPosition(Vector2 tilePos)
        {
            // Check if the position is within bounds
            int posX = (int)tilePos.X;
            int posY = (int)tilePos.Y;
            if (posX >= 0
                && posY >= 0
                && posX < floorTiles.GetLength(0) // gets the length in the X direction
                && posY < floorTiles.GetLength(1)) // gets the array length in the Y direction
            {
                // Yes, this coordinate is legal
                return floorTiles[posX, posY];
            }
            else
            {
                // NO, this coordinate is NOT legal (out of bounds of array / tile grid)
                return null;
            }
        }
        // ------------------
        public void ResetLevel()
        {
            // Delay reloading level until after the update loop
            reloadLevel = true;
            isDead = false;
        }
        // ------------------
        public void Victory()
        {
            player.Kill();
            loadNextLevel = true;
            ourPortal.SetIsVisible(false);
            
        }
        // ------------------
        private void ClearLevel()
        {
            spikes.Clear();
            coins.Clear();
            enemies.Clear();
        }
        // ------------------
        private void PlayerDeath()
        {
            if (player.GetHealth() == 0 )
            {
                if (playerDeathInstance.State != SoundState.Playing)
                {
                    playerDeathInstance.Play();
                }
                isDead = true;
            }

        }
        // ------------------
        /*
        public void SetPlayer(Player newPlayer)
        {
            player = newPlayer;
        }
        
        public void ResetCamera(SpriteBatch spriteBatch)
        {
            ourCamera.Begin(spriteBatch);
        }
        */
    }
}
