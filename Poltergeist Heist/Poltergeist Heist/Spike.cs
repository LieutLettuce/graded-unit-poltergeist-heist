﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;

namespace Poltergeist_Heist
{
    class Spike : Tile
    {
        // ------------------
        // Behaviour
        // ------------------
        public Spike(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
