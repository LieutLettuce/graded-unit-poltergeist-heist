﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Poltergeist_Heist
{
    class TitleScreen : Screen
    {
        // ------------------
        // Data
        // ------------------
        private Text gameName;
        private Text gameName2;
        private Text startPrompt;
        private Game1 game;

        private Texture2D title;
        private Rectangle titleRectangle;
        private Rectangle titleRectangle2;

        // ------------------
        // Behaviour
        // ------------------
        public TitleScreen(Game1 newGame)
        {
            game = newGame;
        }
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largeFont");
            SpriteFont smallFont = content.Load<SpriteFont>("fonts/mainFont");
            title  = content.Load<Texture2D>("graphics/Poltergeist Heist");


            gameName = new Text(titleFont);
            gameName.SetTextString("POLTERGEIST");
            gameName.SetAlignment(Text.Alignment.CENTRE);
            gameName.SetColor(Color.White);
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 100));

            gameName2 = new Text(titleFont);
            gameName2.SetTextString("HEIST");
            gameName2.SetAlignment(Text.Alignment.CENTRE);
            gameName2.SetColor(Color.White);
            gameName2.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2 , 200));

            startPrompt = new Text(smallFont);
            startPrompt.SetTextString("[Press ENTER to Start!]");
            startPrompt.SetAlignment(Text.Alignment.CENTRE);
            startPrompt.SetColor(Color.White);
            startPrompt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 300));
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            //gameName.Draw(spriteBatch);
            
            spriteBatch.Draw(title, titleRectangle, Color.White);
            startPrompt.Draw(spriteBatch);
            //gameName2.Draw(spriteBatch);
            spriteBatch.End();
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            //Check if the player has pressed enter

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // If the player has pressed enter.....

            titleRectangle = new Rectangle(0, 0, 800, 500);

            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                //TODO: CHANGE TO LEVEL SCREEN

                game.ChangeScreen("level");
            }
        }
        // ------------------
    }
}
