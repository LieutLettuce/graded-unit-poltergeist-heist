﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework.Input;

namespace Poltergeist_Heist
{
    class Enemy :  Tile // Player inherits from ("is an") Actor
    {
        // Data;
        private int score = 0;
        private Level levelObject;
        private int lives = 3;
        private Player ourPlayer;

        private int tileX;
        private int tileY;
        private int tileHeight;
        private int tileWidth;

        //Enemy Patrol Data
        private bool right;
        private float distance;
        private float oldDistance;
        private Vector2 velocity;
        private Vector2 origin;
        private float rotation = 0f;

        // Constants
        private const float MOVE_ACCEL = 100000;
        private const float MOVE_DRAG_FACTOR = 0.4f;
        private const float MAX_MOVE_SPEED = 500;
        private const float MIN_ANIM_SPEED = 10;

        //private Vector2 position = Vector2.Zero;w

        // ------------------
        // Behaviour
        // ------------------
        public Enemy(Texture2D newTexture, Level newLevelObject, float newDistance, Vector2 newPosition)//,
            : base(newTexture)
        {
            //Data
            distance = newDistance;
            position = newPosition;
            oldDistance = distance;
            levelObject = newLevelObject;

        }
        // -------------------------------------
        public override void Update(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            position +=  velocity;
            origin = new Vector2(texture.Width / 2, texture.Height / 2);
            oldDistance = 150;

            if (distance <= 0 )
            {
                right = true;
                velocity.X = 1.5f;
            }
            
            else if (distance >= oldDistance)
            {
                right = false;
                velocity.X = -1.5f;
            }
            
            if (right == true)
                distance += 1;
            else distance -= 1;
 

            base.Update(gameTime);
        }
        // ------------------
        public void HandleCollision(Wall hitWall)
        {
            // Determine collision depth (with direction) and magnitude
            // This tells us which direction to move to exit the collision
            Rectangle enemyBounds = GetBounds();
            Vector2 depth = hitWall.GetCollisionDepth(enemyBounds);

            // If the depth is non-zero, it means we ARE colliding with this wall
            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                // Resolve the collision along the shallow axis, as that is the
                // one we're closer to the edge on and therefore easier to "squeeze out"
                if (absDepthY < absDepthX)
                {
                    // Resolve the collision on the Y axis
                    velocity.Y = velocity.Y + depth.Y;
                }
                else
                {
                    // Resolve the collision on the X axis
                    velocity.X = velocity.X + depth.X;
                }
            }
        }
        // ------------------
        public void HandleCollision(Boulder hitBoulder)
        {
            // Determine collision depth (with direction) and magnitude
            // This tells us which direction to move to exit the collision
            Rectangle enemyBounds = GetBounds();
            Vector2 depth = hitBoulder.GetCollisionDepth(enemyBounds);

            // If the depth is non-zero, it means we ARE colliding with this wall
            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                // Resolve the collision along the shallow axis, as that is the
                // one we're closer to the edge on and therefore easier to "squeeze out"
                if (absDepthY < absDepthX)
                {
                    // Resolve the collision on the Y axis
                    position.Y = position.Y + depth.Y;
                }
                else
                {
                    // Resolve the collision on the X axis
                    position.X = position.X + depth.X;
                }
            }
        }
        // ------------------
        public Vector2 GetCollisionDepth(Rectangle otherBounds)
        {
            // This function calculates how far our rectangles are overlapping
            Rectangle tileBounds = GetBounds();

            // Calculate the half sizes of both rectangles
            float halfWidthEnemy = otherBounds.Width / 2.0f;
            float halfHeightEnemy = otherBounds.Height / 2.0f;
            float halfWidthTile = tileBounds.Width / 2.0f;
            float halfHeightTile = tileBounds.Height / 2.0f;

            // Calculate the centers of each rectangle
            Vector2 centrePlayer = new Vector2(otherBounds.Left + halfWidthEnemy,
                                                otherBounds.Top + halfHeightEnemy);
            Vector2 centreTile = new Vector2(tileBounds.Left + halfWidthTile,
                                             tileBounds.Top + halfHeightTile);

            // How far away are the centres of each of these rectangles from eachother
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            // Minimum distance these need to be to NOT collide / intersect
            // If EITHER the X or the Y distance is greater than these minima, these are NOT intersecting
            float minDistanceX = halfWidthEnemy + halfWidthTile;
            float minDistanceY = halfHeightEnemy + halfHeightTile;

            // If we are not intersecting at all, return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
            {
                return Vector2.Zero;
            }

            // Calculate and return the intersection depth
            // Essentially, how much over the minimum intersection distance are we in each direction
            // AKA by how much are they intersecting in that direction
            float depthX = 0;
            float depthY = 0;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;
            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }
        // ------------------

        public void HandleCollision(Spike hitSpike)
        {
            // Kill the player
            // TEMP
            Kill();
        }
        // ------------------
        public int GetLives()
        {
            return lives;
        }
        // ------------------
        public Vector2 GetPosition()
        {
            return position;
        }
        // ------------------
        public void Kill()
        {
            visible = false;
        }
        // -------------------------------------
    }

}