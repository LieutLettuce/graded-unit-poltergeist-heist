﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework.Input;

namespace Poltergeist_Heist
{
    class Portal : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Player player;
        private Level ourLevel;
        private bool isVisible = false;


        private SoundEffect portalSFX;
        private SoundEffectInstance portalSFXInstance;

        // ------------------
        // Behaviour
        // ------------------
        public Portal(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            portalSFX = content.Load<SoundEffect>("audio/portalSound");  // Load The sound for the portal when the player's score hits 100;

            portalSFXInstance = portalSFX.CreateInstance(); // This is the instance for the portal's SFX
            portalSFXInstance.IsLooped = false;
            portalSFXInstance.Volume = 0.1f;
        }
        // -------------------------------------
        public override void Update(GameTime gameTime)
        {
            if (player.GetScore() == 100)
            {
                isVisible = true;
                
                if (portalSFXInstance.State != SoundState.Playing && portalSFXInstance.IsLooped ==false)
                {
                    
                    portalSFXInstance.Play();
                }
            }
        }
        // ------------------
        public Vector2 GetCollisionDepth(Rectangle otherBounds)
        {
            // This function calculates how far our rectangles are overlapping
            Rectangle tileBounds = GetBounds();

            // Calculate the half sizes of both rectangles
            float halfWidthPlayer = otherBounds.Width / 2.0f;
            float halfHeightPlayer = otherBounds.Height / 2.0f;
            float halfWidthTile = tileBounds.Width / 2.0f;
            float halfHeightTile = tileBounds.Height / 2.0f;

            // Calculate the centers of each rectangle
            Vector2 centrePlayer = new Vector2(otherBounds.Left + halfWidthPlayer,
                                                otherBounds.Top + halfHeightPlayer);
            Vector2 centreTile = new Vector2(tileBounds.Left + halfWidthTile,
                                             tileBounds.Top + halfHeightTile);

            // How far away are the centres of each of these rectangles from eachother
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            // Minimum distance these need to be to NOT collide / intersect
            // If EITHER the X or the Y distance is greater than these minima, these are NOT intersecting
            float minDistanceX = halfWidthPlayer + halfWidthTile;
            float minDistanceY = halfHeightPlayer + halfHeightTile;

            // If we are not intersecting at all, return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
            {
                return Vector2.Zero;
            }

            // Calculate and return the intersection depth
            // Essentially, how much over the minimum intersection distance are we in each direction
            // AKA by how much are they intersecting in that direction
            float depthX = 0;
            float depthY = 0;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX ;
            if (distanceY > 0)
                depthY = minDistanceY  - distanceY ;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }
        // ------------------
        public bool GetisVisible()
        {
            return isVisible;
        }
        // ------------------
        public void SetIsVisible(bool newIsVisible)
        {
            isVisible = newIsVisible;
        }
        public void SetPlayer(Player newPlayer)
        {
            player = newPlayer;
        }
    }
}
