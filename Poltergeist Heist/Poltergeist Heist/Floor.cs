﻿using Microsoft.Xna.Framework.Graphics;

namespace Poltergeist_Heist
{
    class Floor : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Floor(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
