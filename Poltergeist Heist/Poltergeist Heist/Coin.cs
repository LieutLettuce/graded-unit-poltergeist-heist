﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;

namespace Poltergeist_Heist
{
    class Coin : Tile
    {
        // Data
        private int scoreValue = 100;


        // ------------------
        // Behaviour
        // ------------------
        public Coin(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
        public int GetScoreValue()
        {
            return scoreValue;
        }
        // ------------------
    }
}
